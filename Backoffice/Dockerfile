﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 5000

ENV ASPNETCORE_URLS=http://+:5000

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY ["Backoffice/Backoffice.csproj", "Backoffice/"]
RUN dotnet restore "Backoffice/Backoffice.csproj"
COPY . .
WORKDIR "/src/Backoffice"
RUN dotnet build "Backoffice.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Backoffice.csproj" -c Release -o /app/publish

FROM node:18-alpine AS node-build
WORKDIR /node
COPY Backoffice/ClientApp /node
RUN npm install
RUN npm run build

FROM base AS final
WORKDIR /app
RUN mkdir /app/ClientApp
RUN mkdir /app/ClientApp/build
COPY --from=publish /app/publish .
copy --from=node-build /node/build /app/ClientApp/build
ENTRYPOINT ["dotnet", "Backoffice.dll"]
