using System.Threading.Tasks;
using Backoffice.Application;
using Backoffice.Infrastructure;
using CommonLibraries.Guards;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;

namespace Backoffice.Controllers
{
    [Route("[controller]")]
    public class EventsController : Controller
    {
        public EventsController(EventService eventService)
        {
            _eventService = Check.NotNull(eventService, nameof(eventService));
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile eventsFile)
        {
            await _eventService.CreateOrUpdateEventsAsync(CsvMapper.MapEvents(eventsFile.OpenReadStream()));
            return Ok();
        }

        private readonly EventService _eventService;
    }
}