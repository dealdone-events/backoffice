using System.Threading.Tasks;
using Backoffice.Infrastructure;
using Backoffice.Infrastructure.Models.File;
using CommonLibraries.Guards;

namespace Backoffice.Application
{
    public class EventService
    {
        public EventService(IEventRepository eventRepository)
        {
            _eventRepository = Check.NotNull(eventRepository, nameof(eventRepository));
        }

        public async Task CreateOrUpdateEventsAsync(Event[] events)
        {
            await _eventRepository.UpdateOrCreateEventsAsync(events);
        }

        private readonly IEventRepository _eventRepository;
    }
}