import React, {Component} from 'react';
import {EventFilter} from './EventFilter';


import "./css/Events.css"

export class Events extends Component {
    render() {
        return (
            <div>
                <br/>
                <h1>Events</h1>
                <br/>
                <EventFilter/>
            </div>
        );
    }
}
