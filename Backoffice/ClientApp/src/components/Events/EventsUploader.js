import React, {Component, useState} from 'react';
import axios from "axios";
import {Button, CustomInput} from "reactstrap";
import Papa from 'papaparse';

import "./css/Events.css"

export class EventsUploader extends Component {
    state = {
        isUploadButtonActive: false,
        csvResults: null,
        selectedFile: null
    };

    onFileChange = fileEvent => {
        if(fileEvent.target.files[0].name.split('.')[1] === 'csv') {
            let file = fileEvent.target.files[0];
            Papa.parse(fileEvent.target.files[0], {
                header: true,
                skipEmptyLines: true,
                complete: (results) => {
                    this.updateContextWithCsvResult(results, file);
                },
            })
        } else {
            this.setState({
                isUploadButtonActive: false,
                csvResults: null,
                selectedFile: null
            })
        }
    };

    updateContextWithCsvResult(results, file){
        this.setState({
            isUploadButtonActive: true,
            csvResults: results.data,
            selectedFile: file
        })
    }

    onFileUpload = () => {
        const formData = new FormData();

        formData.append(
            "eventsFile",
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        console.log(this.state.selectedFile);

        axios.post("events/upload", formData).then(respone => {
            console.log(respone.data);
        });
    };

    renderTable() {
        return(
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Currency</th>
                    <th>Priority</th>
                    <th>Date</th>
                    <th>Attributes</th>
                    <th>Is_active</th>
                    <th>CommonId</th>
                    <th>Id</th>
                </tr>
                </thead>
                <tbody>
                {this.state.csvResults.map((result,index) =>
                    <tr>
                        <td>{index}</td>
                        <td>{result.Title}</td>
                        <td>{result.Description}</td>
                        <td>{result.Currency}</td>
                        <td>{result.Priority}</td>
                        <td>{result.Date}</td>
                        <td>{result.Attributes}</td>
                        <td>{result.IsActive}</td>
                        <td>{result.CommonId}</td>
                        <td>{result.Id}</td>
                    </tr>
                )}
                </tbody>
            </table>
        )
    }

    fileData = () => {
        if (this.state.selectedFile) {
            return (
                <div>
                    <h2>Информация о файле:</h2>
                    <p>File Name: {this.state.selectedFile.name}</p>
                    <p>File Type: {this.state.selectedFile.type}</p>
                    <p>
                        Last Modified:{" "}
                        {this.state.selectedFile.lastModifiedDate.toDateString()}
                    </p>
                    {this.renderTable()}
                </div>
            );
        } else {
            return (
                <div>
                    <br/>
                    <h4>Выберите csv файл перед тем, как загрузить</h4>
                </div>
            );
        }
    };

    render() {
        return (
            <div>
                <h3>
                    Загрузка событий
                </h3>
                <br/>
                <div className="event-container">
                    <div className="event-element">
                        <CustomInput id='0' onChange={this.onFileChange} type="file"/>
                    </div>
                    <div className="event-btn">
                        <Button color="primary" disabled={!this.state.isUploadButtonActive}  onClick={this.onFileUpload}>Загрузить</Button>
                    </div>
                </div>
                <br/>
                {this.fileData()}
            </div>
        );
    }
}