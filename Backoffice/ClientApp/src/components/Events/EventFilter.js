import React, {Component} from 'react';
import {Button, Input, InputGroup, InputGroupAddon} from "reactstrap";

import "./css/Events.css"

export class EventFilter extends Component {
    render() {
        return (
            <div className="event-container">
                <div className="event-element">
                    <InputGroup>
                        <Input placeholder="Название события"/>
                    </InputGroup>
                </div>
                <div className="event-element">
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">С</InputGroupAddon>
                        <Input type="date"/>
                        <InputGroupAddon addonType="prepend">До</InputGroupAddon>
                        <Input type="date"/>
                    </InputGroup>
                </div>
                <div className="event-btn">
                    <Button color="primary">Найти события</Button>
                </div>
                <br/>
            </div>
        );
    }
}