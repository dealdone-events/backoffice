import React, {Component} from 'react';
import {Route, Switch} from 'react-router';
import {Layout} from './components/Layout';
import {Events} from './components/Events/Events';
import {FetchData} from './components/FetchData';
import {Counter} from './components/Counter';
import {Error} from './components/Error'
import {EventsUploader} from "./components/Events/EventsUploader";

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path='/events' component={Events}/>
                    <Route exact path='/eventsUploader' component={EventsUploader}/>
                    <Route exact path='/workspaces' component={Counter}/>
                    <Route exact path='/users' component={FetchData}/>
                    <Route path='/' component={Error}/>
                </Switch>
            </Layout>
        );
    }
}
