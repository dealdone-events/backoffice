using Backoffice.Application;
using Backoffice.ApplicationConfiguration;
using Backoffice.Infrastructure;
using CommonLibraries.Guards;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RockLib.Configuration.ObjectFactory;

namespace Backoffice
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = Check.NotNull(configuration, nameof(configuration));
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var settings = _configuration.Create<ApplicationSettings>();

            services.AddControllersWithViews();
            services.AddLogging();
            
            services.AddScoped<EventService>();
            services.AddScoped<IEventRepository>(_ => new EventRepository(settings.RepositorySettings.ConnectionString));

            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/build"; });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

        private readonly IConfiguration _configuration;
    }
}