using System.Threading.Tasks;
using Backoffice.Infrastructure.Models;
using Backoffice.Infrastructure.Models.File;

namespace Backoffice.Infrastructure
{
    public interface IEventRepository
    {
        Task UpdateOrCreateEventsAsync(Event[] events);
    }
}