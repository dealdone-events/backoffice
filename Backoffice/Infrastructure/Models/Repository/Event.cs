using System;
using System.ComponentModel.DataAnnotations;
using CommonLibraries.Guards;

namespace Backoffice.Infrastructure.Models.Repository
{
    public class Event
    {
        public Event(Guid id,
            Guid? commonId,
            string title,
            string description,
            Currency currency,
            Priority priority,
            DateTime date,
            bool isActive)
        {
            Id = id;
            CommonId = commonId;
            Title = Check.NotNullOrEmpty(title, nameof(title));
            Description = Check.NotNullOrEmpty(description, nameof(description));
            Currency = currency;
            Priority = priority;
            Date = date;
            IsActive = isActive;
        }

        public void Update(Guid? commonId,
            string title,
            string description,
            Currency currency,
            Priority priority,
            DateTime date,
            bool isActive)
        {
            CommonId = commonId;
            Title = Check.NotNullOrEmpty(title, nameof(title));
            Description = Check.NotNullOrEmpty(description, nameof(description));
            Currency = currency;
            Priority = priority;
            Date = date;
            IsActive = isActive;
        }

        [Key] public Guid Id { get; }

        public Guid? CommonId { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public Currency Currency { get; private set; }

        public Priority Priority { get; private set; }

        public DateTime Date { get; private set; }

        public bool IsActive { get; private set; }
    }
}