﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Backoffice.Infrastructure.Models.Repository
{
    public class EventAttribute
    {
        public EventAttribute(Guid id,Attribute attribute, Guid eventId)
        {
            Id = id;
            Attribute = attribute;
            EventId = eventId;
        }

        [Key] public Guid Id { get; }
        
        public Attribute Attribute { get; }
        
        public Guid EventId { get; }
    }
}