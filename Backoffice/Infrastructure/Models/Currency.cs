﻿namespace Backoffice.Infrastructure.Models
{
    public enum Currency
    {
        USD = 1,
        RUB = 2,
        EUR = 3,
        GBP = 4,
        CHF = 5,
        CAD = 6,
        NZD = 7,
        AUD = 8,
        INR = 9,
        BRL = 10,
        KRW = 11,
        TRY = 12,
        JPY = 13,
        CNY = 14,
        ZAR = 15
    }
}