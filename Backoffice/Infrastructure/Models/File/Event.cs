using System;

namespace Backoffice.Infrastructure.Models.File
{
    public class Event
    {
        public Guid? Id { get; set; }
        
        public Guid? CommonId { get; set; }
        
        public string Title { get; set; }
        
        public string Description { get; set; }
        
        public Currency Currency { get; set; }
        
        public Priority Priority { get; set; }
        
        public DateTime Date { get; set; }

        public bool IsActive { get; set; }
        
        public Attribute[] Attributes { get; set; }
    }
}