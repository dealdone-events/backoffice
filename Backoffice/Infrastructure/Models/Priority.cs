﻿namespace Backoffice.Infrastructure.Models
{
    public enum Priority
    {
        High = 2,
        Medium = 1,
        Low = 0
    }
}