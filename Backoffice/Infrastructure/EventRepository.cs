using System;
using System.Linq;
using System.Threading.Tasks;
using Backoffice.Infrastructure.Models.Repository;
using CommonLibraries.Guards;
using CommonLibraries.Repository;
using Microsoft.EntityFrameworkCore;
using Attribute = Backoffice.Infrastructure.Models.Attribute;

namespace Backoffice.Infrastructure
{
    public class EventRepository : DbContext, IEventRepository
    {
        public DbSet<Event> Events { get; set; }

        public DbSet<EventAttribute> EventAttributes { get; set; }

        public EventRepository(string connectionString)
        {
            _connectionString = Check.NotNull(connectionString, nameof(connectionString));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Event>()
                .ToTable("event");

            modelBuilder.Entity<EventAttribute>()
                .ToTable("event_attribute");

            modelBuilder.Entity<Event>()
                .Property(e => e.Id)
                .HasColumnType("CHAR(36)");

            modelBuilder.Entity<Event>()
                .Property(e => e.CommonId)
                .HasColumnName("common_id")
                .HasColumnType("CHAR(36)");

            modelBuilder.Entity<Event>()
                .Property(e => e.Title)
                .HasColumnType("TEXT");

            modelBuilder.Entity<Event>()
                .Property(e => e.Description)
                .HasColumnType("TEXT");

            modelBuilder.Entity<Event>()
                .Property(e => e.Currency)
                .HasColumnType("INT");

            modelBuilder.Entity<Event>()
                .Property(e => e.Priority)
                .HasColumnType("INT");

            modelBuilder.Entity<Event>()
                .Property(e => e.Date)
                .HasColumnType("DATETIME");

            modelBuilder.Entity<Event>()
                .Property(e => e.IsActive)
                .HasColumnName("is_active")
                .HasColumnType("TINYINT(1)");
            
            modelBuilder.Entity<EventAttribute>()
                .Property(e => e.Id)
                .HasColumnType("CHAR(36)");

            modelBuilder.Entity<EventAttribute>()
                .Property(e => e.Attribute)
                .HasColumnName("attribute_id")
                .HasColumnType("INT");

            modelBuilder.Entity<EventAttribute>()
                .Property(e => e.EventId)
                .HasColumnName("event_id")
                .HasColumnType("CHAR(36)");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(_connectionString, ServerVersion.AutoDetect(_connectionString));
        }


        public async Task UpdateOrCreateEventsAsync(Models.File.Event[] events)
        {
            foreach (var @event in events)
            {
                if (@event.Id.HasValue)
                {
                    await UpdateEventAsync(@event);
                }
                else
                {
                    await CreateEventAsync(@event);
                }
            }
            
            await SaveChangesAsync();
        }

        private async Task UpdateEventAsync(Models.File.Event @event)
        {
            var repositoryEvent = await Events.FindAsync(@event.Id);

            if (repositoryEvent == null)
            {
                throw new RepositoryException($"Event with {@event.Id} does not exists");
            }

            repositoryEvent.Update(
                @event.CommonId,
                @event.Title,
                @event.Description,
                @event.Currency,
                @event.Priority,
                @event.Date,
                @event.IsActive);

            Events.Update(repositoryEvent);

            var eventAttributes = EventAttributes.Where(eventAttribute => eventAttribute.EventId == @event.Id);
            var removed = eventAttributes.Where(eventAttribute => !@event.Attributes.Contains(eventAttribute.Attribute));
            var newAttributes = @event.Attributes.Where(attribute => !eventAttributes.Any(eventAttribute => eventAttribute.Attribute == attribute));
            
            foreach (var newAttribute in newAttributes)
            {
                await CreateEventAttributeAsync(newAttribute, repositoryEvent);
            }
            EventAttributes.RemoveRange(removed);
        }

        private async Task CreateEventAsync(Models.File.Event @event)
        {
            var repositoryEvent = new Event(GenerateEventGuid(),
                @event.CommonId,
                @event.Title,
                @event.Description,
                @event.Currency,
                @event.Priority,
                @event.Date,
                @event.IsActive);

            var eventAttributes = @event.Attributes.Select(attribute => new EventAttribute(GenerateEventGuid(),attribute, repositoryEvent.Id));

            await Events.AddAsync(repositoryEvent);
            await EventAttributes.AddRangeAsync(eventAttributes);
        }

        private async Task CreateEventAttributeAsync(Attribute attribute, Event @event)
        {
            await EventAttributes.AddAsync(new EventAttribute(GenerateEventGuid(),attribute, @event.Id));
        }
        
        private Guid GenerateEventGuid()
        {
            var eventGuid = Guid.NewGuid();

            while (Events.Any(repositoryEvent => repositoryEvent.Id == eventGuid))
            {
                eventGuid = Guid.NewGuid();
            }

            return eventGuid;
        }
        
        private Guid GenerateEventAtributeGuid()
        {
            var eventGuid = Guid.NewGuid();

            while (EventAttributes.Any(repositoryEvent => repositoryEvent.Id == eventGuid))
            {
                eventGuid = Guid.NewGuid();
            }

            return eventGuid;
        }


        private readonly string _connectionString;
    }
}