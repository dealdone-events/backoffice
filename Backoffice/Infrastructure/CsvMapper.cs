using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using Backoffice.Infrastructure.Models;
using Backoffice.Infrastructure.Models.File;
using CsvHelper;
using Attribute = Backoffice.Infrastructure.Models.Attribute;

namespace Backoffice.Infrastructure
{
    public static class CsvMapper
    {
        public static Event[] MapEvents(Stream stream)
        {
            using var streamReader = new StreamReader(stream);
            using var csvReader = new CsvReader(streamReader, CultureInfo.InvariantCulture);

            return csvReader.GetRecords<dynamic>().Select(MapDynamicEvent).ToArray();
        }

        private static Event MapDynamicEvent(dynamic dynamicEvent)
        {
            var @event = new Event();

            var csvEventProperties = ((ExpandoObject)dynamicEvent).Select(x => x).ToArray();
            var eventProperties = typeof(Event).GetProperties();

            foreach (var csvEventProperty in csvEventProperties)
            {
                var property = eventProperties.FirstOrDefault(property => property.Name == csvEventProperty.Key);
                if (property != null)
                {
                    property.SetValue(@event, Converters[property.PropertyType]((string)csvEventProperty.Value));
                }
            }

            return @event;
        }


        private static readonly IDictionary<Type, Func<string, object>> Converters = new Dictionary<Type, Func<string, object>>
        {
            { typeof(string), value => value },
            { typeof(bool), value => bool.Parse(value) },
            { typeof(Guid?), value =>  value == "null" ? null : Guid.Parse(value) },
            { typeof(Currency), value => Enum.Parse<Currency>(value) },
            { typeof(Priority), value => Enum.Parse<Priority>(value) },
            { typeof(DateTime), value => DateTime.Parse(value) },
            { typeof(Attribute[]), value => value.Split(';', StringSplitOptions.RemoveEmptyEntries).Select(Enum.Parse<Attribute>).ToArray() }
        };
    }
}