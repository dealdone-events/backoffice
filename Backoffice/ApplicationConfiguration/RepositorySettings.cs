using CommonLibraries.Guards;

namespace Backoffice.ApplicationConfiguration
{
    public class RepositorySettings
    {
        public RepositorySettings(string connectionString)
        {
            ConnectionString = Check.NotNullOrEmpty(connectionString, nameof(connectionString));
        }

        public string ConnectionString { get; }
    }
}