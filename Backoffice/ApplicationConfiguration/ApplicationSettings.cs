using CommonLibraries.Guards;

namespace Backoffice.ApplicationConfiguration
{
    public class ApplicationSettings
    {
        public ApplicationSettings(RepositorySettings repositorySettings)
        {
            RepositorySettings = Check.NotNull(repositorySettings, nameof(repositorySettings));
        }

        public RepositorySettings RepositorySettings { get; }
    }
}